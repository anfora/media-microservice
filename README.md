# Media microservice

This microservice is written in `Rust` and handles the events related
to the storage of media files and transformations over them.

## Why Rust

Rust provides nice tools to work with videos and images and the
operations related to IO are faster than in Python.


